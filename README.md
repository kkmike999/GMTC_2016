# 全球移动技术大会（GMTC）2016

### [官网](http://gmtc.geekbang.org)
讲师及日程安排

### [大会照片](http://yun.baidu.com/share/link?shareid=3164536659&uk=962589474)

### [演讲视频](http://www.infoq.com/cn/presentations)

---

### 讲师&主题 
![image](http://git.oschina.net/kkmike999/GMTC_2016/raw/master/全球移动技术大会2016%20讲师+主题.png?dir=0&filepath=全球移动技术大会2016+讲师%2B主题.png&oid=a3bda230c12cd7903ad766be693931e24c1ea9ba&sha=11720b16fb7f60cb4de456300524a4abec7d9514)

### 大会日程
![image](http://git.oschina.net/kkmike999/GMTC_2016/raw/master/GMTC全球移动技术大会议程.png?dir=0&filepath=GMTC全球移动技术大会议程.png&oid=c8f5fe3a90f9bc195a2fde4ffd6dfbb3a84dab5c&sha=b14a75fe7893ba6f532132e0a35e193fa3ec5f4d)
